package eu.telecomnancy.sensor;

import eu.telecomnancy.pattern.StateOff;
import eu.telecomnancy.pattern.StateOn;

public class SensorState implements ISensor {
	boolean state;
    double value = 0;
    StateOn sensorOn = new StateOn();
    StateOff sensorOff = new StateOff();
    
	@Override
	public void on() {
		state = sensorOn.getStatus();
	}

	@Override
	public void off() {
		state = sensorOff.getStatus();
	}

	@Override
	public boolean getStatus() {
		return state;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		if(state){
			value = sensorOn.getValue();
		}
		else{
			value = sensorOff.getValue();
		}
			
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (state)
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

}
