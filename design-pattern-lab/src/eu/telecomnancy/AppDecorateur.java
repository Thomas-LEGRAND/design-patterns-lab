package eu.telecomnancy;

import eu.telecomnancy.pattern.DecorateurFahrenheit;
import eu.telecomnancy.pattern.DecorateurValeurArrondit;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppDecorateur {

	public static void main(String[] args) {
		ISensor sensor = new DecorateurFahrenheit(new DecorateurValeurArrondit(new TemperatureSensor()));
        new ConsoleUI(sensor);
	}

}
