package eu.telecomnancy.ui;

import eu.telecomnancy.pattern.CommandeUpdate;
import eu.telecomnancy.pattern.CommandeOn;
import eu.telecomnancy.pattern.CommandeOff;
import eu.telecomnancy.pattern.CommandeGetValue;
import eu.telecomnancy.pattern.DecorateurCelsius;
import eu.telecomnancy.pattern.DecorateurFahrenheit;
import eu.telecomnancy.pattern.DecorateurValeurArrondit;
import eu.telecomnancy.pattern.ICommande;
import eu.telecomnancy.pattern.Invoker;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.TemperatureSensor;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

public class SensorView extends JPanel implements Observer{
    /**
     * 
     * Vue de programme qui observe si la classe TemperatureSensor
     * a été modifié 
     * 
     */
    private static final long serialVersionUID = 1L;

    private ISensor sensor;

    private DecorateurFahrenheit decoFahrenheit;
    private DecorateurValeurArrondit decoArrondit;
    private DecorateurCelsius decoCelsius;

    private Invoker invokRequete = new Invoker();

    private ICommande commandeUpdate;
    private ICommande commandeOn;
    private ICommande commandeOff;
    private ICommande commandeGetValue;

    private JLabel value = new JLabel("N/A °C");

    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    private JRadioButton fahrenheit = new JRadioButton("Conversion fahrenheit");
    private JRadioButton celsius = new JRadioButton("Conversion celsius");

    private JCheckBox arrondi = new JCheckBox("Arrondir valeur");

    public SensorView(ISensor c){
	this.sensor = c;
	this.setLayout(new BorderLayout());

	/*
	 *	Create design pattern decorator 
	 */
	decoFahrenheit = new DecorateurFahrenheit(this.sensor);
	decoArrondit = new DecorateurValeurArrondit(this.sensor);
	decoCelsius = new DecorateurCelsius(this.sensor);

	/*
	 * Create design pattern command
	 */
	commandeUpdate = new CommandeUpdate(this.sensor);
	commandeOn = new CommandeOn(this.sensor);
	commandeOff = new CommandeOff(this.sensor);
	commandeGetValue = new CommandeGetValue(this.sensor);
		
	value.setHorizontalAlignment(SwingConstants.CENTER);
	Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
	value.setFont(sensorValueFont);

	this.add(value, BorderLayout.CENTER);

	/*
	 * On est dans l'observateur à qui on donne un observable qui se traduit par ici
	 * 
	 * 	obersavable.ajouterObervateur(moi)
	 */
	((TemperatureSensor)sensor).addObserver(this);

	on.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
		    //sensor.on();
		    try {
			invokRequete.event(commandeOn);
		    } catch (SensorNotActivatedException e1) {
			e1.printStackTrace();
		    }		
		}
	    });

	off.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
		    //sensor.off();
		    try {
			invokRequete.event(commandeOff);
		    } catch (SensorNotActivatedException e1) {
			e1.printStackTrace();
		    }
		}
	    });

	update.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
		    //sensor.update();
		    try {
			invokRequete.event(commandeUpdate);
		    } catch (SensorNotActivatedException e1) {
			e1.printStackTrace();
		    }
		}
	    });


	celsius.setSelected(true);
	celsius.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
		    if(celsius.isSelected()){
			try {
			    if(arrondi.isSelected())
				value.setText(Double.toString(decoArrondit.getValue()) + " °C");
			    else
				value.setText(Double.toString(decoCelsius.getValue()) + " °C");
			} catch (SensorNotActivatedException e1) {
			    e1.printStackTrace();
			}
		    }
		}
	    });

	fahrenheit.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
		    if(fahrenheit.isSelected()){
			try {
			    if(arrondi.isSelected())
				value.setText(Double.toString(decoArrondit.getValue()) + " °F");
			    else
				value.setText(Double.toString(decoFahrenheit.getValue()) + " °F");
			} catch (SensorNotActivatedException e1) {
			    e1.printStackTrace();
			}
		    }
		}
	    });

	arrondi.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
		    if(arrondi.isSelected()){
			try {
			    if(fahrenheit.isSelected())
				value.setText(Double.toString(decoArrondit.getValue()) + " °F");
			    if(celsius.isSelected())
				value.setText(Double.toString(decoArrondit.getValue()) + " °C");
			} catch (SensorNotActivatedException e1) {
			    e1.printStackTrace();
			}
		    }
		}
	    });



	JPanel buttonsPanel = new JPanel();
	buttonsPanel.setLayout(new GridLayout(1, 3));
	buttonsPanel.add(update);
	buttonsPanel.add(on);
	buttonsPanel.add(off);

	this.add(buttonsPanel, BorderLayout.SOUTH);

	JPanel CheckPanel = new JPanel();
	CheckPanel.setLayout(new GridLayout(1, 3));
	CheckPanel.add(arrondi);
	CheckPanel.add(celsius);
	CheckPanel.add(fahrenheit);

	ButtonGroup group = new ButtonGroup();
	group.add(celsius);
	group.add(fahrenheit);


	this.add(CheckPanel,BorderLayout.NORTH);
    }


    @Override
    public void update(Observable arg0, Object arg1) {
	try {
	    if(fahrenheit.isSelected()){
		decoFahrenheit = new DecorateurFahrenheit((TemperatureSensor)arg0);
		if(arrondi.isSelected()){
		    decoArrondit = new DecorateurValeurArrondit(decoFahrenheit);
		    value.setText(Double.toString(decoArrondit.getValue()) + " °F");
		}
		else{
		    value.setText(Double.toString(decoFahrenheit.getValue()) + " °F");
		}
	    }

	    if(celsius.isSelected()){
		decoCelsius = new DecorateurCelsius((TemperatureSensor)arg0);
		if(arrondi.isSelected()){
		    decoArrondit = new DecorateurValeurArrondit(decoCelsius);
		    value.setText(Double.toString(decoArrondit.getValue()) + " °C");
		}
		else{
		    value.setText(Double.toString(decoCelsius.getValue()) + " °C");
		}
	    }
	} catch (SensorNotActivatedException e) {
	    e.printStackTrace();
	}
    }
}
