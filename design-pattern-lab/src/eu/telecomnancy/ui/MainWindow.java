package eu.telecomnancy.ui;

import eu.telecomnancy.sensor.ISensor;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainWindow extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    static String nameApplication = "Application Design Pattern";

	private ISensor sensor;
    private SensorView sensorView;
    
    private JMenuBar menuBar = new JMenuBar();
	private JMenu menu;
	private JMenuItem item;
	
    public MainWindow(ISensor sensor) {
    	super(nameApplication);
    	this.setSize(400, 400);
        this.sensor = sensor;
        this.sensorView = new SensorView(this.sensor);
      
        this.setLocationRelativeTo(null);
        
        menu = new JMenu("Fichier");
        item = new JMenuItem("Exit",'e');
		item.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(EXIT_ON_CLOSE);

			}
		});
		/** Add component to the pane or to the menu */
		menu.add(item);
        menuBar.add(menu);
        
        this.setJMenuBar(menuBar);
        
        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}
