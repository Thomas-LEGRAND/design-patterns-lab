package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;


public class DecorateurCelsius extends Decorateur{
	
	public DecorateurCelsius(ISensor sensor){
		super(sensor);
	}
	
	public double getValue() throws SensorNotActivatedException{
		return mysensor.getValue();
	}
}
