/**
 * 
 */
package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 * @author tlegrand
 *
 */
public abstract class Decorateur implements ISensor{
	protected ISensor mysensor;
	
	public Decorateur(ISensor sensor){
		this.mysensor = sensor;
	}
	
	public void on(){
		this.mysensor.on();
	}
	
	public void off(){
		this.mysensor.off();
	}
	
	public boolean getStatus() {
		return mysensor.getStatus();
	}
	
	public void update() throws SensorNotActivatedException{
		this.mysensor.update();
	}
}
