package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Invoker {

	public Invoker() {
		
	}
	
	public void event(ICommande cmd) throws SensorNotActivatedException{
		cmd.requete();
	}

}
