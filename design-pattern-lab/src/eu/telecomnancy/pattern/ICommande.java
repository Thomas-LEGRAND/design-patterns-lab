/**
 * 
 */
package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 * @author tlegrand
 *
 */
public interface ICommande {

	public void requete() throws SensorNotActivatedException;
}
