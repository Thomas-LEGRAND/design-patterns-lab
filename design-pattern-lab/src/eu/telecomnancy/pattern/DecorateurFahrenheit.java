package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class DecorateurFahrenheit extends Decorateur {

	public DecorateurFahrenheit(ISensor sensor){
		super(sensor);
	}
	
	public double getValue() throws SensorNotActivatedException{
		return (mysensor.getValue() * 9/5) + 32;
	}
}
