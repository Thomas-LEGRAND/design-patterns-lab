package eu.telecomnancy.pattern;

public class StateOff extends State{
	@Override
	public boolean getStatus() {
		return false;
	}
	
	@Override
	public double getValue() {
		return 0;
	}
}
