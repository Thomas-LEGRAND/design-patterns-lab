
package eu.telecomnancy.pattern;

/**
 * @author tlegrand
 *
 */
public abstract class State{
	abstract double getValue();
	
	abstract boolean getStatus();
}
