package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.SensorNotActivatedException;
import eu.telecomnancy.sensor.ISensor;

public class CommandeGetValue implements ICommande {

	private ISensor mysensor;
	
	public CommandeGetValue(ISensor sensor) {
		this.mysensor = sensor;
	}

	@Override
	public void requete() throws SensorNotActivatedException {
		this.mysensor.getValue();
	}

}
