package eu.telecomnancy.pattern;

import java.util.Random;

public class StateOn extends State{
	@Override
	public boolean getStatus(){
		return true;
	}
	
	@Override
	public double getValue() {
		return (new Random()).nextDouble() * 100;
	}
}
