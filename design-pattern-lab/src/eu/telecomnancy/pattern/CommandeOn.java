/**
 * 
 */
package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.ISensor;


/**
 * @author tlegrand
 *
 */
public class CommandeOn implements ICommande {
	private ISensor mysensor;
	
	public CommandeOn(ISensor mySensor2) {
		this.mysensor = mySensor2;
	}
	
	public void requete(){
		this.mysensor.on();
	}

}
