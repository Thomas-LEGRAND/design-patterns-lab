package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class AdaptateurTemperatureSensor implements ISensor{
	LegacyTemperatureSensor adapte;
	
	public AdaptateurTemperatureSensor(LegacyTemperatureSensor adapte){
		this.adapte = adapte;
	}
	
	@Override
    public void on() {
      if(!adapte.getStatus()){
    	  adapte.onOff();
      }
    }

    @Override
    public void off() {
    	if(adapte.getStatus()){
      	  adapte.onOff();
        } 
    }

	@Override
	public void update() throws SensorNotActivatedException {
		return;	
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		if (adapte.getStatus())
            return adapte.getTemperature();
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

	@Override
	public boolean getStatus() {
		return adapte.getStatus();
	}

}
