/**
 * 
 */
package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

/**
 * @author tlegrand
 *
 */
public class DecorateurValeurArrondit extends Decorateur{
	
	public DecorateurValeurArrondit(ISensor sensor){
		super(sensor);
	}
	
	public double getValue() throws SensorNotActivatedException{
		return Math.round(mysensor.getValue());
	}
}
