/**
 * 
 */
package eu.telecomnancy.pattern;

import eu.telecomnancy.sensor.ISensor;

/**
 * @author tlegrand
 *
 */
public class CommandeOff implements ICommande {
	private ISensor mysensor;
	/**
	 * 
	 */
	public CommandeOff(ISensor mySensor2) {
		this.mysensor = mySensor2;
	}
	
	public void requete(){
		this.mysensor.off();
	}

}
