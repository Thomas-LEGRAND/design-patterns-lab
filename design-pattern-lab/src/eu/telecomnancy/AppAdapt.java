package eu.telecomnancy;

import eu.telecomnancy.pattern.AdaptateurTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppAdapt {

	public static void main(String[] args) {
        ISensor sensor = new AdaptateurTemperatureSensor(new LegacyTemperatureSensor());
        new ConsoleUI(sensor);
    }

}
